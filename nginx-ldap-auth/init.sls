{% from slspath + "/map.jinja" import nginx_ldap_auth with context %}

{# Install dependencies #}
git package is installed:
  pkg.installed:
    - name: git

virtualenv is installed:
  pkg.installed:
    - pkgs:
      - virtualenv
      - python-virtualenv  
      # Dependencies for the pip requirements
      - build-essential
      - python-dev
      - libldap2-dev
      - libsasl2-dev

application group is present:
  group.present:
    - name: nginx-ldap-auth
    - system: True

application user is present:
  user.present:
    - name: nginx-ldap-auth
    - home: /var/lib/nginx-ldap-auth
    - groups:
      - nginx-ldap-auth
    - system: True
    - require:
      - group: nginx-ldap-auth

{# Create the service directories #}
application directory is present:
  file.directory:
    - name: /var/lib/nginx-ldap-auth/code
    - user: nginx-ldap-auth
    - group: nginx-ldap-auth
    - require:
      - user: nginx-ldap-auth
      - group: nginx-ldap-auth


{# Clone the code repository #}
deploy app code:
  git.latest:
    - name: {{ nginx_ldap_auth.repository }}
    - target: /var/lib/nginx-ldap-auth/code
    - user: nginx-ldap-auth
    - branch: {{ nginx_ldap_auth.branch }}
    - force_clone: True
    - force_reset: True
    - force_checkout: True
    - require:
      - user: nginx-ldap-auth
      - pkg: git

{# Prepare the runtime #}
deploy app runtime:
  virtualenv.managed:
    - name: /var/lib/nginx-ldap-auth/runtime
    - user: nginx-ldap-auth
    - python: python2
    - system_site_packages: False
    - requirements: /var/lib/nginx-ldap-auth/code/requirements.txt
    - require:
      - git: deploy app code
      - pkg: virtualenv is installed

{# Link the binary #}
link the binary:
  file.symlink:
    - target: /var/lib/nginx-ldap-auth/code/nginx-ldap-auth-daemon.py
    - name: /usr/bin/nginx-ldap-auth-daemon

{# Deploy the required configuration #}
deploy environment file:
  file.managed:
    - name: /etc/default/nginx-ldap-auth
    - user: nginx-ldap-auth
    - group: nginx-ldap-auth
    - chmod: 0400
    - contents: |
        # Salt managed configuration
        {%- for entry, value in nginx_ldap_auth.environment.items() %}
        {{ entry | upper }}={{ value }}
        {%- endfor %}

deploy config file:
  file.managed:
    - name: /var/lib/nginx-ldap-auth/code/config.json
    - user: nginx-ldap-auth
    - group: nginx-ldap-auth
    - chmod: 0400
    - contents: |
        {{ nginx_ldap_auth.config | json | indent(8) }} 

{# Setup the system service #}
deploy app systemd unit:
  file.managed:
    - name: /etc/systemd/system/nginx-ldap-auth.service
    - source: /var/lib/nginx-ldap-auth/code/nginx-ldap-auth.service
    - template: jinja

amend working directory in systemd unit:
  file.replace:
    - name: /etc/systemd/system/nginx-ldap-auth.service
    - pattern: |
        WorkingDirectory=/var/run
    - repl: |
        WorkingDirectory=/var/lib/nginx-ldap-auth/code
    - require:
      - file: deploy app systemd unit

amend exec start in systemd unit:
  file.replace:
    - name: /etc/systemd/system/nginx-ldap-auth.service
    - pattern: /usr/bin/nginx-ldap-auth-daemon
    - repl: /var/lib/nginx-ldap-auth/runtime/bin/python /var/lib/nginx-ldap-auth/code/nginx-ldap-auth-daemon.py
    - require:
      - file: deploy app systemd unit

reload systemd unit:
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: amend exec start in systemd unit
      - file: amend working directory in systemd unit

enable application service:
  service.running:
    - name: nginx-ldap-auth
    - enable: True
    - restart: True
    - require:
      - file: link the binary
    - watch:
      - virtualenv: /var/lib/nginx-ldap-auth/runtime
      - file: deploy environment file
      - file: deploy config file
      - module: reload systemd unit