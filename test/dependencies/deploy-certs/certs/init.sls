{# Deploy Deposit Solution authorities onto test machine #}

deploy intermediate:
  file.managed:
    - name: /usr/local/share/ca-certificates/deposit-intermediate.crt
    - source: salt://certs/files/intermediateca.pem

deploy root:
  file.managed:
    - name: /usr/local/share/ca-certificates/deposit-root.crt
    - source: salt://certs/files/rootca.pem

trust-certificates:
  cmd.run:
    - name: update-ca-certificates
    - order: first
    - require:
      - file: deploy intermediate
      - file: deploy root