===============
nginx-ldap-auth
===============
.. image:: https://git.prod.infra.deposit/saltstack/nginx-ldap-auth-formula/badges/master/build.svg
    :target: https://git.prod.infra.deposit/saltstack/nginx-ldap-auth-formula/pipelines
.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``nginx-ldap-auth``
------------

Installs and configures the nginx-ldap-auth service.

.. vim: fenc=utf-8 spell spl=en cc=100 tw=99 fo=want sts=4 sw=4 et
